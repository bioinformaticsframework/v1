# Bioinformatics Framework

The Bioinformatics Framework project aims to make genomics tools more accessible to scientists without unix/sys-admin experience. We plan to do this by

1. Promoting the use of docker to significantly reduce time to install and configure tools
1. Creating an intuitive user interface targeted toward bioinformatitions