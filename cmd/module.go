package cmd

import (
	"fmt"
	"github.com/apcera/termtables"
	"github.com/spf13/cobra"
	"gitlab.com/bioinformaticsframework/v1/core/module"
)

// Maybe this should be called use
var loadCmd = &cobra.Command{
	Use:   "load",
	Short: "Loads a module from a path relative to the modules directory",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := module.LoadModule(args[0])
		if err != nil {
			fmt.Println(err)
		}
	},
}

var showCmd = &cobra.Command{
	Use:   "show",
	Short: "Shows description and options related to the current active module",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		activeModule, err := module.GetModule()
		if err != nil {
			fmt.Println(err)
		} else {
			options := activeModule.ModuleOptions
			fmt.Println(activeModule.Name)
			fmt.Println(activeModule.Author)
			fmt.Println(activeModule.Description)
			fmt.Println()
			table := termtables.CreateTable()
			table.AddHeaders("Name", "Description", "Default", "Required", "AllowedValues", "Value")
			for _, option := range options {
				table.AddRow(option.Name, option.Description, option.Default, option.Required, option.AllowedValues, option.Value)
			}
			fmt.Println(table.Render())
		}
	},
}

var setOptionCmd = &cobra.Command{
	Use:   "set",
	Short: "Sets an option",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		err := module.SetOption(args[0], args[1])
		if err != nil {
			fmt.Println(err)
		}
	},
}

var listModulesCmd = &cobra.Command{
	Use:   "list",
	Short: "Lists modules",
	Run: func(cmd *cobra.Command, args []string) {
		moduleNames, err := module.ListModules()
		if err != nil {
			fmt.Println(err)
		}
		for _, moduleName := range moduleNames {
			fmt.Println(moduleName)
		}
	},
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run the current module",
	Run: func(cmd *cobra.Command, args []string) {
		activeModule, err := module.GetModule()
		if err != nil {
			fmt.Println(err)
		} else {
			err = module.RunModule(activeModule)
			if err != nil {
				fmt.Println(err)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(loadCmd)
	rootCmd.AddCommand(showCmd)
	rootCmd.AddCommand(setOptionCmd)
	rootCmd.AddCommand(runCmd)
	rootCmd.AddCommand(listModulesCmd)
}
