// Copyright © 2018 Jake Berkowsky

package cmd

import (
	"fmt"
	"os"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var baseDir string

var rootCmd = &cobra.Command{
	Use:   "bif",
	Short: "The Bioinformatics framework",
}

// Execute cli
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&baseDir, "base-dir", "", "base directory (default is $HOME/bif/)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	// Find home directory.
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Set base directory
	if baseDir == "" {
		baseDir = os.Getenv("BIF_BASEDIR")
		if baseDir == "" {
			baseDir = home + "/bif"
		}
	}
	viper.Set("base_dir", baseDir)

	viper.SetConfigType("yaml")
	viper.AddConfigPath(baseDir)
	viper.SetConfigName("config") // .yml .yaml

	viper.SetEnvPrefix("BIF")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	//if err := viper.ReadInConfig(); err == nil {
	//	fmt.Println("Using config file:", viper.ConfigFileUsed())
	//}

	//Set some defaults
	viper.SetDefault("module_dir", baseDir)
}
