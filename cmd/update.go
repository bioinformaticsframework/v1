package cmd

import (
	"bufio"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
)

var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "update modules",
	Run: func(cmd *cobra.Command, args []string) {
		if !cmd.Flag("force").Changed {
			reader := bufio.NewReader(os.Stdin)
			fmt.Print("This command will update (overwrite) modules at ", viper.Get("base_dir"), "/modules \n")
			fmt.Print("Continue? (y/N): ")
			input, _ := reader.ReadString('\n')
			if input != "y\n" {
				os.Exit(0)
			}
		}
		fmt.Println("Initializing")
	},
}

func init() {
	rootCmd.AddCommand(updateCmd)
	updateCmd.Flags().Bool("force", false, "disable confirmation prompt")
}
