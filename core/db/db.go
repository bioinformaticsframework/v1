package db

import (
	"errors"
	"github.com/coreos/bbolt"
	"github.com/spf13/viper"
	"time"
)

var moduleRootBucketName = "Module"
var stateBucketName = "State"

// SaveOption will save an option and its value to a bucket
func SaveOption(moduleName string, optionName string, optionValue string) error {
	baseDir := viper.GetString("base_dir")
	db, err := bolt.Open(baseDir+"/state.db", 0600, &bolt.Options{Timeout: 1 * time.Second})

	if err != nil {
		return err
	}

	defer db.Close() //nolint: errcheck

	err = db.Update(func(tx *bolt.Tx) error {
		ModuleRootBucket, error := tx.CreateBucketIfNotExists([]byte(moduleRootBucketName))
		if error != nil {
			return error
		}
		ModuleBucket, error := ModuleRootBucket.CreateBucketIfNotExists([]byte(moduleName))
		if error != nil {
			return error
		}
		OptionsBucket, error := ModuleBucket.CreateBucketIfNotExists([]byte("options"))
		if error != nil {
			return error
		}
		error = OptionsBucket.Put([]byte(optionName), []byte(optionValue))
		return error
	})
	return err
}

// GetOptions returns options of a module with values
func GetOptions(moduleName string) (map[string]string, error) {
	baseDir := viper.GetString("base_dir")
	db, err := bolt.Open(baseDir+"/state.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return nil, err
	}

	defer db.Close() //nolint: errcheck

	options := make(map[string]string)
	err = db.View(func(tx *bolt.Tx) error {
		ModuleRootBucket := tx.Bucket([]byte(moduleRootBucketName))
		if ModuleRootBucket == nil {
			return nil
		}
		ModuleBucket := ModuleRootBucket.Bucket([]byte(moduleName))
		if ModuleBucket == nil {
			return nil
		}
		OptionsBucket := ModuleBucket.Bucket([]byte("options"))
		if OptionsBucket == nil {
			return nil
		}
		cursor := OptionsBucket.Cursor()
		for k, v := cursor.First(); k != nil; k, v = cursor.Next() {
			options[string(k)] = string(v)
		}
		return nil
	})
	return options, err

}

// SaveActiveModule sets the active module
//maybe this should be save state, but right now there is no state besides active module
func SaveActiveModule(moduleName string) error {
	baseDir := viper.GetString("base_dir")
	db, err := bolt.Open(baseDir+"/state.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return err
	}
	defer db.Close() //nolint: errcheck

	err = db.Update(func(tx *bolt.Tx) error {
		StateBucket, error := tx.CreateBucketIfNotExists([]byte(stateBucketName))
		if error != nil {
			return error
		}
		error = StateBucket.Put([]byte("active_module"), []byte(moduleName))
		return error
	})
	return err
}

// GetActiveModule returns the name of the active module
func GetActiveModule() (string, error) {
	baseDir := viper.GetString("base_dir")
	db, err := bolt.Open(baseDir+"/state.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return "", err
	}
	defer db.Close() //nolint: errcheck

	activeModule := ""
	err = db.View(func(tx *bolt.Tx) error {
		StateBucket := tx.Bucket([]byte(stateBucketName))
		if StateBucket == nil {
			// The state bucket doesn't exist, which also means the active module isn't loaded
			return errors.New("Active module not set ")
		}
		moduleName := StateBucket.Get([]byte("active_module"))
		if moduleName == nil {
			return errors.New("Active module not set ")
		}
		activeModule = string(moduleName)
		return nil
	})
	return activeModule, err
}
