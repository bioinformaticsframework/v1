package module

import (
	"github.com/docker/docker/api/types/mount"
	"github.com/valyala/fasttemplate"
)

// DockerOptions - options for docker
type DockerOptions struct {
	Image   string
	Command []string
	Mounts  []Mount
}

// Mount - User settings for a mount
type Mount struct {
	Source string
	Target string
}

func (module Module) hasRequiredDockerOptions() bool {
	// For now just checks image
	dockerOptions := module.DockerOptions
	return dockerOptions.Image == ""
}

func (module Module) interpolate() DockerOptions {
	var interpolatedOptions DockerOptions
	var variables = make(map[string]interface{})
	for _, option := range module.ModuleOptions {
		variables[option.Name] = option.Value
	}
	interpolatedOptions.Image = fasttemplate.New(module.DockerOptions.Image, "{{", "}}").ExecuteString(variables)
	for _, commandPart := range module.DockerOptions.Command {
		interpolatedOptions.Command = append(interpolatedOptions.Command, fasttemplate.New(commandPart, "{{", "}}").ExecuteString(variables))
	}
	for _, mountInfo := range module.DockerOptions.Mounts {
		interpolatedMount := Mount{
			Source: fasttemplate.New(mountInfo.Source, "{{", "}}").ExecuteString(variables),
			Target: fasttemplate.New(mountInfo.Target, "{{", "}}").ExecuteString(variables),
		}
		interpolatedOptions.Mounts = append(interpolatedOptions.Mounts, interpolatedMount)
	}
	return interpolatedOptions
}

func (dockerOptions DockerOptions) getDockerMounts() []mount.Mount {
	var dockerMounts []mount.Mount
	for _, dockerMount := range dockerOptions.Mounts {
		dockerMounts = append(dockerMounts, mount.Mount{
			Type:   mount.TypeBind,
			Source: dockerMount.Source,
			Target: dockerMount.Target,
		})
	}
	return dockerMounts
}
