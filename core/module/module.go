package module

import (
	"errors"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/spf13/viper"
	"gitlab.com/bioinformaticsframework/v1/core/db"
	"golang.org/x/net/context"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"path/filepath"
	"strings"
)

// Option stores info about a module option
type Option struct {
	Name          string
	Required      string
	Description   string
	Default       string
	AllowedValues []string
	Value         string
}

// Module is the main struct for a module
type Module struct {
	Version       string
	Type          string
	Name          string
	Author        string
	Description   string
	Tags          []string
	ModuleOptions []Option      `yaml:"module_options"`
	DockerOptions DockerOptions `yaml:"docker_options"`
}

// ListModules - Lists modules
func ListModules() ([]string, error) {
	var moduleNames []string

	baseDir := viper.GetString("base_dir")
	files, err := ioutil.ReadDir(baseDir + "/modules/")
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		moduleName := strings.TrimSuffix(file.Name(), filepath.Ext(file.Name()))
		moduleNames = append(moduleNames, moduleName)
	}
	return moduleNames, nil
}

// LoadModule validates and sets the active module
func LoadModule(path string) error {
	baseDir := viper.GetString("base_dir")
	modulePath := filepath.FromSlash(baseDir + "/modules/" + path + ".yml")
	// Check that module is valid
	if !validateModule(modulePath) {
		return errors.New("invalid module")
	}
	_, err := parseModule(modulePath)
	if err != nil {
		return err
	}
	err = db.SaveActiveModule(path)
	return err
}

func (module Module) isReadyToRun() bool {
	for _, moduleOption := range module.ModuleOptions {
		if (moduleOption.Required != "") && (moduleOption.Value == "") {
			return false
		}
	}
	return module.hasRequiredDockerOptions()
}

func (module Module) hasOption(optionName string) bool {
	moduleOptions := module.ModuleOptions
	for _, option := range moduleOptions {
		if optionName == option.Name {
			return true
		}
	}
	return false
}

// GetModule returns an array of options for the current active module
func GetModule() (Module, error) {
	// Check if a module is loaded
	moduleName, err := db.GetActiveModule()
	if err != nil {
		return Module{}, err
	}
	baseDir := viper.GetString("base_dir")
	modulePath := filepath.FromSlash(baseDir + "/modules/" + moduleName + ".yml")
	module, err := parseModule(modulePath)
	if err != nil {
		return Module{}, err
	}
	moduleOptions := module.ModuleOptions

	// Parse the DB to try to fill in the gaps
	dbOptions, err := db.GetOptions(moduleName)
	for i, moduleOption := range moduleOptions {
		if dbOptionValue, ok := dbOptions[moduleOption.Name]; ok {
			moduleOptions[i].Value = dbOptionValue
		}
		// Todo: Add logic for the default value
	}

	if err != nil {
		return Module{}, err
	}
	// return the options as some sort of data structure
	return module, nil
}

// SetOption sets an option for the currently active module
func SetOption(optionName string, optionValue string) error {
	moduleName, err := db.GetActiveModule()
	if err != nil {
		return err
	}
	baseDir := viper.GetString("base_dir")
	modulePath := filepath.FromSlash(baseDir + "/modules/" + moduleName + ".yml")
	module, err := parseModule(modulePath)
	// Todo: add check if allowed option
	if err != nil {
		return err
	}

	if !module.hasOption(optionName) {
		return errors.New(optionName + " not found in active module ")
	}

	return db.SaveOption(moduleName, optionName, optionValue)
}

// RunModule - Runs the active module
func RunModule(module Module) error {
	//Check if module has all required options filled in
	if !module.isReadyToRun() {
		return errors.New("module is missing required options")
	}
	println("Running")
	ctx := context.Background()
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}
	_, err = cli.ImagePull(ctx, module.DockerOptions.Image, types.ImagePullOptions{})
	if err != nil {
		panic(err)
	}
	interpolatedDockerOptions := module.interpolate()
	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image: interpolatedDockerOptions.Image,
		Cmd:   interpolatedDockerOptions.Command,
	}, &container.HostConfig{
		Mounts: interpolatedDockerOptions.getDockerMounts(),
	}, nil, "")

	if err != nil {
		panic(err)
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

	fmt.Println(resp.ID)

	return nil
}

func validateModule(path string) bool {
	_, err := parseModule(path)
	return err == nil
}

func parseModule(path string) (Module, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return Module{}, err
	}
	module := Module{}
	err = yaml.Unmarshal(data, &module)
	if err != nil {
		return Module{}, errors.New("Cannot parse module: " + path)
	}
	return module, nil
}
