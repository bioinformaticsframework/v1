// Copyright © 2018 Jake Berkowsky

package main

import "gitlab.com/bioinformaticsframework/v1/cmd"

func main() {
	cmd.Execute()
}
